package info.igreque.the.haskellioinjava;

import java.util.concurrent.Callable;
import java.util.function.Function;

/**
 * HaskellのIOをそれっぽくシミュレートするクラス。
 * 詳細は http://the.igreque.info/posts/2017/01-haskell-io-in-java.html を参照。
 */

public class IO<T1> {
  private final Callable<T1> internalAction;

  IO(Callable<T1> internalAction){
    this.internalAction = internalAction;
  }

  public <T2> IO<T2> plus(IO<T2> nextIo) {
    return new IO<>(() -> {
      internalAction.call();
      return nextIo.internalAction.call();
    });
  }

  public <T2> IO<T2> then(Function<T1, IO<T2>> makeNextIo) {
    return new IO<>(() -> {
      T1 result = internalAction.call();
      IO<T2> nextIo = makeNextIo.apply(result);
      return nextIo.internalAction.call();
    });
  }

  /**
   * 実際に実行するために便宜上必要。
   * internalActionがprivateなため、このクラスに呼び出すメソッドが必要となる。
   *
   * @param mainIo Haskellでいうところの「main関数」に該当する{@code IO}オブジェクト。
   *               今回のサンプルでは、各クラスのpublic staticなプロパティ main を渡すことで使う。
   */
  static void runMain(IO<Void> mainIo) throws Exception {
    mainIo.internalAction.call();
  }
}
