#!/usr/bin/env stack
-- stack --resolver=lts-7.2 runghc


main :: IO ()
main = do
  putStrLn "Nice to meet you!"
  putStrLn "May I have your name? "
  name <- getLine
  putStrLn ("Your name is " ++ name ++ "?")
  putStrLn "Nice name!"
