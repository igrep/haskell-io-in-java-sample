# haskell-io-in-java

このリポジトリーでは、[HaskellのIOは他の言語でいうところの関数オブジェクトとよく似てるよ、という話](http://the.igreque.info/posts/2017/01-haskell-io-in-java.html)で紹介した「Haskellの`IO`を無理矢理Javaに翻訳したクラス」と、それを使ったサンプルコードが含まれています。  
そのまま動作するコードとなっておりますので、[IOSample](src/main/java/info/igreque/the/haskellioinjava/IOSample.java)などを書き換えて試しに[RunIOSample](src/main/java/info/igreque/the/haskellioinjava/RunIOSample.java)を実行して動作させてみると、「Haskellにおいて`IO`は組み合わせるものだ」という世界観をより強く実感できるかもしれません。

詳細な仕組みを理解する場合は、下記のような順番で読むとよいかもしれません。

1. [IO.java](src/main/java/info/igreque/the/haskellioinjava/IO.java): [元記事](http://the.igreque.info/posts/2017/01-haskell-io-in-java.html)にも書いた、Haskellの`IO`をJavaでシミュレートしたものです。Prelude.javaやIOSample.javaでライブラリーとして使用します。
1. [IOSampleInOrdinaryJava.java](src/main/java/info/igreque/the/haskellioinjava/IOSampleInOrdinaryJava.java): 上記の`IO`を使ったサンプルプログラムを、普通のJavaで書いた場合のプログラムです。比較対照としてご覧ください。
1. [Prelude.java](src/main/java/info/igreque/the/haskellioinjava/Prelude.java): Haskellの標準ライブラリーに入っている、`Prelude`というモジュールの一部を再現しました。ここで定義した定数をIOSample.javaで使用します。
1. [IOSample.java](src/main/java/info/igreque/the/haskellioinjava/IOSample.java): `IO`を使ったサンプルプログラムです。IOSampleInOrdinary.javaをPrelude.javaが提供する`IO`アクションをIO.javaが提供するAPIで、「組み合わせて」います。
1. [RunIOSample.java](src/main/java/info/igreque/the/haskellioinjava/RunIOSample.java): IOSample.javaを実際にJavaのプログラムとして実行するプログラムです。Javaプログラムとしての`main`メソッドはこちらにあります。

また、参考までに、[IOSample.javaと等価なプログラムのHaskell版](src/main/hs/io-sample.hs)など、[元記事](http://the.igreque.info/posts/2017/01-haskell-io-in-java.html)で言及したHaskell製のプログラムについても、このリポジトリーに含まれています。  
[src/main/hs ディレクトリー](src/main/hs)をご覧ください。

# LICENSE

一応MITライセンスとしておきます。  
詳細は[LICENSEファイル](LICENSE)をご覧ください。
